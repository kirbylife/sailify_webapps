# Sailify webapps (cli)

Make your webapps icons in the app box look better with the rest of your native apps by choosing whether the corners are rounded or pointed

## Dependencies
- SailfishOS (tested in SailfishOS 4 with an Xperia XA2)
- [Python3-numpy](https://openrepos.net/content/nobodyinperson/python3-numpy)
- [Python3-pillow](https://openrepos.net/content/birdzhang/python3-pillow)

## Usage
- Clone the repository or download the sailify_webapps.py on your phone
- On a terminal (or under SSH) type "python3 sailify_webapps.py"
- The program will show you the webapps you currently have on your device, select the one you want to change the icon for
- 4 numbers (0 or 1) depending on whether you want the corner rounded or pointed
  - if you write 1001 the upper left corner and the lower right corner will be pointed and the rest will be rounded
  - if you write 1111 all the corners will end in a point (they will make a perfect square)
  - if you type 0100 the upper right corner will end in a point and the rest will be rounded

## Images
### Amazon example
![Amazon instructions](/git_images/Amazon_instructions.jpg)
![Amazon result](/git_images/Amazon_result.jpg)
### Uber example
![Uber instructions](/git_images/Uber_instructions.jpg)
![Uber result](/git_images/Uber_result.jpg)

## TO-DO
- [ ] fault tolerant
- [x] create a [GUI](https://gitlab.com/kirbylife/harbour-muchkin)
- [ ] option to restore the original icon

## Disclaimer
This is a very earlier version of this software and the results are not waranted, use it under your own responsability
This program create an .desktop_backup inside of ~/.local/share/applications/ that containing an intact version of the original icon

Contributors are welcome :)
